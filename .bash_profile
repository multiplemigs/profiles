# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

export LSCOLORS='gxBxhxDxfxhxhxhxhxcxcx'
export EDITOR="vim"

export PS1="\[\e[0;32m\][\u\[\e[m\]\[\e[0;38m\]@\[\e[m\]\[\e[1;35m\]\h\[\e[m\] \[\e[0;33m\]\W\[\e[m\]\[\e[0;32m\]]$ \[\e[m\]"
export SUDO_PS1="\[\e[1;31m\][\u\[\e[m\]\[\e[0;38m\]@\[\e[m\]\[\e[1;35m\]\h\[\e[m\] \[\e[0;33m\]\W\[\e[m\]\[\e[1;31m\]]$ \[\e[m\]"

#source ~/.profile
export PATH=/usr/local/bin:$PATH

function h() {
	if [ -z "$1" ]
	then
		history
	else
		history | grep "$@"
	fi
}

function pwstat() {
	if [[ -e ~/.pdump/..pwc ]]; then
		echo "Last event was a pull"
	else
		echo "Last event was a push"
	fi
}

#for nIH only
#alias goj="ssh jumphost"
#alias goa="ssh -i ~/.ssh/alfred_rsa alfred@nimhNDARint.nimh.nih.gov"

#these are commands to get to jmx console
#jconsole nimhndarstage2.nimh.nih.gov:9010

#function jc() {
#	if [ -z "$1" ]
#	then
#		echo "give me a hostname you derp"
#	else
#		jconsole "$@:9010"
#	fi
#}

#for osx only
#defaults write com.apple.Dock autohide-delay -float 9999999 && killall Dock
#
#export HISTSIZE=1000
#export HISTFILESIZE=1000

#function mousi() {
#	while true; do cliclick -w 200 m:+20,+0 && cliclick -w 200 m:-20,+0;done
#}
